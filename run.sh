#!/bin/bash
set -eu

echo -e " \e[1;34m******************************************************************************************\e[0m "
echo "   
                      _               _            _   _             
                     (_) _ _    ___ | |_   __ _ | | | | ___   _ _ 
                     | | | ' \ (_-< | _| / _ | | | | | / -_) | '_|
                     |_| |_||_| /__/ \__| \__,_| |_| |_| \___| |_|  
"
echo -e " \e[1;34m******************************************************************************************\e[0m "
echo ""

echo "Listando clusters" 
echo "============================================================================================================"
clusters=$(aws eks list-clusters)
if [ $? -ne 0 ]; then
    echo "Error al listar clusters. Por favor, verifica tu configuración de AWS."
    exit 1
fi
echo "$clusters"
echo "============================================================================================================"
echo ""

echo "Actualizando kubeconfig" 
echo "============================================================================================================"
aws eks update-kubeconfig --name cluster-EKS-1
if [ $? -ne 0 ]; then
    echo "Error al actualizar kubeconfig. Por favor, verifica el nombre del cluster."
    exit 1
fi
echo "============================================================================================================"
echo ""

echo "Información del kubeconfig" 
echo "============================================================================================================"
cluster_info=$(aws eks describe-cluster --name eks-cluster)
if [ $? -ne 0 ]; then
    echo "Error al obtener información del cluster. Por favor, verifica el nombre del cluster."
    exit 1
fi
echo "$cluster_info"
echo "============================================================================================================"
echo ""

echo "Contexto actual" 
echo "============================================================================================================"
current_context=$(kubectl config current-context)
if [ $? -ne 0 ]; then
    echo "Error al obtener el contexto actual. Por favor, verifica tu configuración de kubectl."
    exit 1
fi
echo "$current_context"
echo "============================================================================================================"
echo ""

echo "Aplicamos el manifest que crea nuestro namespace" 
echo "============================================================================================================"
kubectl apply -f namespace.yaml
if [ $? -ne 0 ]; then
    echo "Error al aplicar el manifest para el namespace. Por favor, verifica el archivo namespace.yaml."
    exit 1
fi
echo "============================================================================================================"
echo ""

echo "Obteniendo namespace" 
echo "============================================================================================================"
namespaces=$(kubectl get namespaces)
if [ $? -ne 0 ]; then
    echo "Error al obtener los namespaces. Por favor, verifica tu configuración de kubectl."
    exit 1
fi
echo "$namespaces"
echo "============================================================================================================"
echo ""

echo "Aplicamos el manifest que despliega nuestro deployment" 
echo "============================================================================================================"
kubectl apply -f deployment.yaml
if [ $? -ne 0 ]; then
    echo "Error al aplicar el manifest para el deployment. Por favor, verifica el archivo deployment.yaml."
    exit 1
fi
echo "============================================================================================================"
echo ""

echo "Controlamos que nuestro deployment ya esté listo" 
echo "============================================================================================================"
deployment_status=$(kubectl -n nombreNameSpace get deployment)
if [ $? -ne 0 ]; then
    echo "Error al obtener el estado del deployment. Por favor, verifica el nombre del namespace."
    exit 1
fi
echo "$deployment_status"

deployment_status=$(kubectl -n cluster-eks-1 get deployment)
if [ $? -ne 0 ]; then
    echo "Error al obtener el estado del deployment. Por favor, verifica el nombre del namespace."
    exit 1
fi
echo "$deployment_status"

echo "Descripción del deployment" 
echo "============================================================================================================"
deployment_description=$(kubectl -n nombreNameSpace describe deployment clase4)
if [ $? -ne 0 ]; then
    echo "Error al describir el deployment. Por favor, verifica el nombre del namespace y el nombre del deployment."
    exit 1
fi
echo "$deployment_description"
