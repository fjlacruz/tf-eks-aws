#=========================================================================================#
#======================= 1. Proceso de creación de la VPC ================================#
#=========================================================================================#
# Crear la VPC
resource "aws_vpc" "VPC-EKS-TF" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "VPC-EKS-TF"
  }
}
# Crear las subnets públicas
resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.VPC-EKS-TF.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-1"
  }
}
resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.VPC-EKS-TF.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-2"
  }
}
resource "aws_subnet" "public_subnet_3" {
  vpc_id                  = aws_vpc.VPC-EKS-TF.id
  cidr_block              = "10.0.3.0/24"
  availability_zone       = "us-east-1c"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-3"
  }
}
# Crear el Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.VPC-EKS-TF.id

  tags = {
    Name = "TF-EKS-igw"
  }
}
# Configurar las tablas de rutas
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.VPC-EKS-TF.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "public-route-table"
  }
}
# Asociar las subnets a las tablas de rutas correspondientes
resource "aws_route_table_association" "public_subnet_association_1" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_route_table.id
}
resource "aws_route_table_association" "public_subnet_association_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_route_table.id
}
resource "aws_route_table_association" "public_subnet_association_3" {
  subnet_id      = aws_subnet.public_subnet_3.id
  route_table_id = aws_route_table.public_route_table.id
}

#=========================================================================================#
#======================= 2. Crear rol de ejecución para EKS ==============================#
#=========================================================================================#
# Crear rol e iam de ejecución para EKS
resource "aws_iam_role" "eks_cluster_role" {
  name               = "role_eks"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}
#Asociacion de politicas de Eks al rol creado
resource "aws_iam_role_policy_attachment" "eks_cluster_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_cluster_role.name
}

resource "aws_iam_role_policy_attachment" "eks_service_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks_cluster_role.name
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly-EKS" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_cluster_role.name
}

#=========================================================================================#
#================== 3. Crear grupo de seguridad para cluster =============================#
#=========================================================================================#
resource "aws_security_group" "sg_eks_tf" {
  name        = "sg_eks_tf"
  description = "Grupo de seguridad para EKS"
  vpc_id      = aws_vpc.VPC-EKS-TF.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Permite acceso desde cualquier dirección IP en Internet
  }

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Permite acceso desde cualquier dirección IP en Internet
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "sg_eks_tf"
  }
}

#=========================================================================================#
#================== 4. Crear cluster EKS =================================================#
#=========================================================================================#
resource "aws_eks_cluster" "eks_cluster" {
  name     = "cluster-EKS-1"
  role_arn = aws_iam_role.eks_cluster_role.arn
  vpc_config {
    subnet_ids = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]
  }
  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  tags = {
    "env" = "DEV"
  }
}

#=========================================================================================#
#=============== 6. Crear rol de ejecucion para nodos =====================================#
#=========================================================================================#
# Rol de IAM para ejecucion de los workernodes
resource "aws_iam_role" "eks_node_group_role" {
  name               = "eks-node-group-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
  depends_on         = [aws_eks_cluster.eks_cluster]
}
resource "aws_iam_role_policy_attachment" "eks_node_cluster_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_node_group_role.name
  depends_on = [aws_eks_cluster.eks_cluster]
}

resource "aws_iam_role_policy_attachment" "eks_node_cluster_CNIpolicy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_node_group_role.name
  depends_on = [aws_eks_cluster.eks_cluster]
}

resource "aws_iam_role_policy_attachment" "EC2InstanceProfileForImageBuilderECRContainerBuilds" {
  policy_arn = "arn:aws:iam::aws:policy/EC2InstanceProfileForImageBuilderECRContainerBuilds"
  role       = aws_iam_role.eks_node_group_role.name
  depends_on = [aws_eks_cluster.eks_cluster]
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_node_group_role.name
  depends_on = [aws_eks_cluster.eks_cluster]
}

#=========================================================================================#
#================== 5. Asignar nodos al cluster EKS ======================================#
#=========================================================================================#
resource "aws_eks_node_group" "node_group" {
  cluster_name    = aws_eks_cluster.eks_cluster.name
  node_group_name = "node-group-1"
  node_role_arn   = aws_iam_role.eks_node_group_role.arn
  subnet_ids      = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]

  scaling_config {
    desired_size = 2 # Ajusta según la cantidad deseada de nodos
    max_size     = 3 # Ajusta según el tamaño máximo permitido
    min_size     = 1 # Ajusta según el tamaño mínimo permitido
  }
  ami_type       = "AL2_x86_64"
  instance_types = ["t3.small"]
  disk_size      = 20

  depends_on = [
    aws_iam_role_policy_attachment.eks_node_cluster_policy_attachment,
    aws_iam_role_policy_attachment.eks_node_cluster_policy_attachment,
    #aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]

}