terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.65.0"
    }
  }
  required_version = "~>1.3.5" # ~ se crea con alt + 126
}

provider "aws" {
  # Configuration options
  region = "us-east-1"
}
